#!/bin/sh
LOGGER="/usr/bin/logger -t"
ZROOT="zroot"
ZFS="/sbin/zfs "
RECDEPTH=1

# Seven day ago
# -------------
SEVEND=`date -j -v"-7d" +"%s"`

# lookup for snapshots older than 7 days
for i in `${ZFS} list -d ${RECDEPTH}-H -t snap -o name -r ${ZROOT}` ; do
	SNAPCREATED=`${ZFS} list -Hp -t snap -o creation ${i}`
	if [ ${?} -eq 0 ]; then
		di=`expr ${SNAPCREATED} - ${SEVEND}`
		if [ ${di} -lt 0 ]; then
			echo "removing " ${i}
			${ZFS} destroy -r ${i}
		fi
	fi
done

echo "Creating snapshot tree at "${ZROOT}"@"${SNAPDATE}
# snap it
${ZFS} snap -r ${ZROOT}@${SNAPDATE} > /dev/null 2>&1
if [ ${?} -ne 0 ]; then
	echo  "unable to create a snapshot for ${ZROOT} at ${SNAPDATE}."
fi

