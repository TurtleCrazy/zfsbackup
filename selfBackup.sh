#!/bin/sh
ZROOT="zroot"
SSH="/usr/bin/ssh"
ZFS="/sbin/zfs "
REMOTE="matarje"
REMOTE_USER="david"
REMOTE_KEY="my/zfs_id_rsa"
REMOTE_POOL="zaway/BCK/master/louison"

# getting last snapshot id
# if one cannot trust the snap id one can use `creation` time
#

ZLAST=`${ZFS} list -o name -Hp -t snap -d 1 ${ZROOT} | cut -d '@' -f 2 | sort | tail -1` 
REMOTE_ZLAST=`${SSH} -i ${REMOTE_KEY} ${REMOTE_USER}@${REMOTE} ${ZFS} list -o name -Hp -t snap -r ${REMOTE_POOL}/ROOT/master | cut -d '@' -f 2 | sort | tail -1` 


echo "sending incremental backup from " ${ZLAST}" to "${REMOTE_ZLAST}

${ZFS} send -R -i ${ZROOT}@${REMOTE_ZLAST} ${ZROOT}@${ZLAST} | ${SSH} -i  ${REMOTE_KEY} ${REMOTE_USER}@${REMOTE} ${ZFS} recv -Fduv ${REMOTE_POOL}
